### FLASK REVIEW 

## Configuración de Entorno

1. Creando un entorno virtual
1. Instalación de paquetes (flask, pythion-dotenv)
2. Crea variables de entorno.

## Manejando Rutas

1. Generando URLs
2. URL´s dinámicas con Flask
3. Redireccionamiento de rutas y error: 
4. Creación de URL's (url_for)
5. Métodos HTTP en nuestra rutas

## Renderizando HTML: Jinja2: se aprenderá a renderizar HTML desde flask, rarchivos estaticos, heredar plantillas y mostrar mensajes flashin

1. Introducción a Jinja2.
2. Renderizando nuestra primera página.
3. Manejando archivos estáticos.
4. Generando HTML dinámico con JINJA2
5. Mensajes Flashes (Error, Éxito, Información)
6. Heredando plantillas (DRY) 

## Manejando Formularios y Peticiones: Flask-WTForms
1. Introducción a Flask-WTForms
2. Creando clases LoginForm
3. Renderizando Formularios
4. Procesando formualrios Flask-WTForms
5. Agregando estilos a los formularios.

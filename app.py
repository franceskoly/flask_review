from flask import Flask, url_for, redirect, abort, request
from markupsafe import escape, Markup


app = Flask(__name__)

""" @app.route("/")
def index():
    return "Hola noa canoa XD"

@app.route("/user/<name>")
def login(name):
    template = Markup("Hello - <em>{}</em>")
    return template.format(name) """


@app.route('/saludo')
@app.route('/saludo/<name>')
@app.route('/saludo/<name>/<int:age>')
def saludo(name=None, age=None):

    if name is not None:
        
        if age is not None:

            return Markup("Hola {}, age: {}").format(name, age)

        return """
            <h1 style=" color:blue">Hola {0}</h1>
            """.format(name, age)

    
    return """
        <h1 style=" color:blue">Hola Desconocido </h1>
        """.format(name, age)

@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    # se usa el tipo de convertidor path
    return f"Subpath {escape(subpath)}"

## Redireccionamiento de rutas: 
### redirect: redirige atraves de URL a un usuario de una pagina a otra
### url_for: redirige un usuario de de una pagina a otra a traves de funciones

""" @app.route('/')
def index():
    return redirect(('/login')) # regirigiendo con URL

@app.route('/login/<name>')
@app.route('/login')

def login(name = None):

    if name is not None:
        return redirect(f"/dashboard/{name}") # redirigiendo a traves de url

    return redirect(url_for('dashboard')) # redirigiendo a traves de funciones


@app.route('/dashboard/<name>')
@app.route('/dashboard')
def dashboard(name=None):

    if name:
        return f"Bienvenido {name} al dashboard "
    return f"Bienvenido Dsconocido... XD"
 """

# Creacion de URL(url_for) 

""" @app.route('/')
def index():
    print(url_for('index'))
    return 'index'

@app.route('/login/<name>')
@app.route('/login')
def login(name=None):
    return redirect(url_for('perfil', name=name )) # redireccionando url con funciones -  argumentos


@app.route('/user/<name>')
@app.route('/user')
def perfil(name = None):
    if name is not None:
        return f"Bienvenido {name} al perfil de ususario"
    return f"Bienvenido desconocido al perfil de usuario"
 """

## Métodos HTTP: Las aplicacion web utilizan diferenete métodos HTTP al acceder a las URL, se usa el argumento "methods" del 
## decorador route para manejar diferentes metodos HTTP.

## Flask proporciona un atajo; get(), post() para cada método HTTP com

""" @app.route('/login', methods=['GET', 'POST'])
def login():

    if request.method == "POST":
        return f"do the login"
    return f"show the login form" """

""" @app.get('/login')
def g_login():
    return f"show the login form"

@app.post('/login')
def p_login():
    return f"do the login" """


## Ejemplo login utilizando los conceptos 1-4
@app.route('/login', methods= ['GET', 'POST'])
def login():
    
    if request.method == 'POST':
        usenname = request.form['name']
        password =  request.form['password']
        print(usenname, password)

        if usenname and password:
            return redirect(url_for("dashboard", name=usenname))
        
    return """
            <form method='POST'>
            <h3>Ingrese sus datos</h3>  
            <input type="text" name=name></input><br>
            <input type="password" name=password></input><br>
            <button type="submit">Enviar</button>
            </form>
            """
        

@app.route('/dashboard')
@app.route('/dashboard/<name>')
def dashboard(name = None):
    
    if name is not None:
        return f"Bienvenido al dashboard {name}"
    
    return redirect(url_for("login"))
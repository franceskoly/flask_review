from flask import Flask, render_template, request, redirect, url_for, flash, session
from forms import LoginForm

app = Flask(__name__)
app.secret_key= "noacanoa" # para trabajar con mensajes flashes es requerido mensajes secretos


""" @app.route('/hello')
@app.route('/hello/<name>')
def index(name=None):
    names = ["paolo", "pedro", "noa", "filomena", "zulema", "jordy"]
    return render_template('hello.html', name=name, family=names)
 """

@app.route('/')
def index():
    return render_template('index.html') 
@app.route('/contacto')
def contacto():
    return render_template('index.html') 
@app.route('/nosotros')
def nosotros():
    return render_template('index.html') 


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    form = LoginForm(request.form) # con request.form: captura los datos ingresados

    print(form.validate_on_submit())
    if request.method == 'POST' and form.validate_on_submit():
        name = request.form['username']
        password = request.form['password']
        if name and password == 'noacanoa':  ## se accede a dashboard solo si password es igual "noacanoa"
            flash("You were successfully logged in", "success")
            session["username"] = name
            return redirect(url_for('dashboard', name = name))
        
        flash("Credenciales inválidas", "warning") # hay categorias de mensajes
        

    return render_template('login.html', form = form)
    

@app.route('/profile')
@app.route('/profile/<name>')
def dashboard(name=None):
    if name is not None:
        return render_template('dashboard.html', name=name)
    return redirect('/login')

@app.route('/logout')
def logout():

    session.clear()  ## eliminando la sesion
    flash("Usuario ha cerrado sesión")
    return redirect(url_for('login'))

## Jinja2: es un motor de plantilla rapido, expresivo y extensivo.
## Tiene marcadores de posicion especial parecidos a Python

# 1. Archivos estáticos: Archivos que complementan las plantillas; estas iran en una carpeta llamada static

# 2. Plantillas de renderizado: Jinja2 genera las plantillas HTML, una forma de mostrarlos es a traves de render_template

# 3. render_template: permite generar como respuesa una plantilla en el texto
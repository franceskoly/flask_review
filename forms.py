from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Length, EqualTo

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(message="Ingrese su username"), Length(min=3, max=60)])
    password = PasswordField('Password', validators = [DataRequired(message="Ingrese su Password"), Length(min=3)])
    submit = SubmitField("Submit")

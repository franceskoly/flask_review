DROP TABLE IF EXISTS parcelas;
DROP TABLE IF EXISTS puestos;

select * from parcelas;
select * from puestos;

CREATE TABLE parcelas (
	id_parcela SERIAL PRIMARY KEY NOT NULL,
	name VARCHAR (90) NOT NULL,
	location VARCHAR (120) NOT NULL,
	area DECIMAL(4,2) NOT NULL,
	number_cosecha INTEGER NOT NULL,
	created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE puestos (
	id_puesto SERIAL PRIMARY KEY NOT NULL,
	id_parcela INTEGER,
	side CHAR (1) NOT NULL,
	n_puesto INTEGER NOT NULL,
	cantidad INTEGER NOT NULL,
	CONSTRAINT FK_PARCELAS FOREIGN KEY (id_parcela) REFERENCES parcelas (id_parcela)
)

insert into parcelas (name, location, area, number_cosecha) values 
('El Cajamarquino', 'Jose Carlos Mariategui',20.0, 1 )


insert into puestos (id_parcela, side, n_puesto, cantidad) values 
(1, 'A', 1, 125),
(1, 'A', 2, 250)